<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            .body{
                font-family: Arial, Helvetica, sans-serif;
                font-style: bold;
            }
            .container1{
                background-color: #eeeeee;
                width: 850px;
                height: 160px;
                margin-top: 20px;
            }
            .container{
                margin-left: 250px;
                width: :850px;
            }
            .headd{
                font-size: 30pt;
                font-family: Arial;
                padding: 65px 0 35px 0;
            }
            .ban{
                width: 850px;
                height: 40px;
                background-color: #ea6a5b;
                
            }
            
            ul{
                list-style-type: none;
            }
            li a {
                display: inline-block;
            }
            a{
                text-decoration: none;
                font-family: Arial;
            }
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
            }

            .topnav {
            overflow: hidden;
            }

            .topnav a:hover {
            color: #333;
            }

            .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 10px 12px;
            text-decoration: none;
            font-size: 11pt;
            }
            .topnav a.active {
                color:black;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <center>
           <div class="container1">
               <center>
                   <h1 class="headd"><b>JUAN'S AUTO PAINT</b></h1>
               </center>
              <div class="ban">
                   <div class="topnav">
                       <B>
                        <a href="/newPaintJob">NEW PAINT JOB</a></li>
                        <a href="/paintJob">PAINT JOBS</a>
                       </B>
                      </div>
               </div>
            </div>
            <br>
            <br>
            @yield('content');
    </center>
    </body>
</html>
