<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //go to index
    public function index(){
        return view('pages.newPaintJob');
    }

    //go to paint job
    public function paintJob(){
        return view('pages.paintJob');
    }
}
